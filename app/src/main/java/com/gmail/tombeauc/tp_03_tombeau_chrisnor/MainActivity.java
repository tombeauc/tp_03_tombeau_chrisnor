package com.gmail.tombeauc.tp_03_tombeau_chrisnor;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private Button buttonGmail;
    private Button buttonAppel;
    private Button buttonGoogle;
    private Button buttonTp01;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initviews();
    }

    public void initviews(){
        buttonGmail = findViewById(R.id.gmail);
        buttonAppel = findViewById(R.id.appel);
        buttonGoogle = findViewById(R.id.google);
        buttonTp01 = findViewById(R.id.tp01);

        buttonGmail.setOnClickListener(new View.OnClickListener() {
            // @Override
            public void onClick(View view) {
                Uri uri = Uri.parse ("http://www.gmail.com");
                Intent intent = new Intent(Intent.ACTION_VIEW,uri);
                intent.setData(Uri.parse("http://www.gmail.com"));
                try {
                    startActivity(intent);
                } catch (ActivityNotFoundException e) {

                }
            }
        });

        buttonAppel.setOnClickListener(new View.OnClickListener() {
            // @Override
            public void onClick(View view) {

                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:+509 38 05 12 74"));
                startActivity(intent);
            }
        });

        buttonGoogle.setOnClickListener(new View.OnClickListener() {
            // @Override
            public void onClick(View view) {
                Uri uri = Uri.parse ("http://www.google.com");
                Intent intent = new Intent(Intent.ACTION_VIEW,uri);
                intent.setData(Uri.parse("http://www.google.com"));
                try {
                    startActivity(intent);
                } catch (ActivityNotFoundException e) {

                }

            }
        });

        buttonTp01.setOnClickListener(new View.OnClickListener() {
            // @Override
            public void onClick(View view) {
                Intent intent = new Intent("gestion.mbds.CALC");
                startActivity(intent);
            }
        });
    }
}